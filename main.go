package main

import (
	"os"

	"github.com/elastic/beats/libbeat/beat"
	"gitlab.com/perigee/pulse/beater"
)

func main() {
	err := beat.Run("pulse", "", beater.New)
	if err != nil {
		os.Exit(1)
	}
}
