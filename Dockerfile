FROM golang:1.10.2-alpine3.7


ARG PROJECT_PATH
ARG LOCAL_UID

ENV PROJECT_PATH $PROJECT_PATH
ENV USER_UID ${LOCAL_UID:-1000}

## Necessary for target program running as linux binary
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 0

RUN mkdir -p ${PROJECT_PATH}
RUN /usr/sbin/adduser -S -u ${USER_UID} usdev

ADD requirements.txt /tmp/requirements.txt
ADD . ${PROJECT_PATH}


RUN apk --update --no-cache add \
    alpine-sdk \
    curl \
    make \
    openssl \
    ca-certificates \
    git \
    python3 \
    py3-netifaces \
    bash \
    && ln -s /usr/bin/python3 /usr/bin/python  \
    && python3 -m ensurepip \
    && rm -r /usr/lib/python*/ensurepip \
    && pip3 install --upgrade pip setuptools \
    && pip install -r /tmp/requirements.txt \
    && pip install virtualenv \
    && update-ca-certificates \
    && rm -r /root/.cache


## Install the golang packages managers (glide used by beats)
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
RUN curl https://glide.sh/get | sh

#RUN dep ensure --add github.com/elastic/beats@v5.6.9

RUN chown -R usdev /go
USER usdev
WORKDIR ${PROJECT_PATH}