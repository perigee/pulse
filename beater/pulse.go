package beater

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/elastic/beats/libbeat/beat"
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/elastic/beats/libbeat/publisher"

	"docker.io/go-docker"
	"docker.io/go-docker/api/types"

	"gitlab.com/perigee/pulse/config"
)

type Pulse struct {
	done          chan struct{}
	config        config.Config
	client        publisher.Client
	lastIndexTime time.Time
}

// Creates beater
func New(b *beat.Beat, cfg *common.Config) (beat.Beater, error) {
	config := config.DefaultConfig
	if err := cfg.Unpack(&config); err != nil {
		return nil, fmt.Errorf("Error reading config file: %v", err)
	}

	bt := &Pulse{
		done:   make(chan struct{}),
		config: config,
	}
	return bt, nil
}

func (bt *Pulse) Run(b *beat.Beat) error {
	logp.Info("pulse is running! Hit CTRL-C to stop it.")

	bt.client = b.Publisher.Connect()
	ticker := time.NewTicker(bt.config.Period)
	//counter := 1
	for {
		select {
		case <-bt.done:
			return nil
		case <-ticker.C:
		}

		checkPass := false

		names, err := bt.checkContainers(bt.config.Containers)
		//bt.lastIndexTime = now

		if err != nil {
			logp.Err(err.Error())
		}

		if names != nil {
			if len(names) < 1 {
				checkPass = true
			}
		} else {
			names = []string{}
		}

		event := common.MapStr{
			"@timestamp": common.Time(time.Now()),
			"type":       b.Name,
			//"counter":    counter,
			"NotRunning": names,
			"passed":     checkPass,
		}
		bt.client.PublishEvent(event)
		logp.Info("Event sent")
		//counter++
	}
}

func (bt *Pulse) Stop() {
	bt.client.Close()
	close(bt.done)
}

//================================
// Check first socket exist or not
//================================
func (bt *Pulse) checkContainers(inCtnames []string) ([]string, error) {
	cli, err := docker.NewEnvClient()

	if err != nil {
		logp.Err("Cannot connect to docker socket")
		return nil, err
	}

	logp.Info("Docker Client Initialized")

	containers, err := cli.ContainerList(
		context.Background(),
		types.ContainerListOptions{},
	)

	if err != nil {
		logp.Err("Cannot fetch containers list")
		return nil, err
	}

	var ctnames []string
	ctnames = make([]string, len(inCtnames))
	copy(ctnames, inCtnames)

	sort.Sort(byLength(ctnames))

	for _, container := range containers {

		logp.Info("%s %s %s\n", container.ID[:10],
			container.Names, container.State)

		for _, name := range container.Names {

			index := -1

			for i, matchname := range ctnames {
				if strings.Contains(name, matchname) {
					logp.Info("\nmatch: %s", matchname)
					index = i
					break
				}
			}

			if index != -1 {
				ctnames[index] = ctnames[len(ctnames)-1]
				ctnames = ctnames[:len(ctnames)-1]
				logp.Info("\nCurrent: %d\n", len(ctnames))
				break
			}

		}
	}

	logp.Info("\n Total: %d", len(ctnames))

	return ctnames, nil
}
