from pulse import BaseTest

import os


class Test(BaseTest):

    def test_base(self):
        """
        Basic test with exiting Pulse normally
        """
        self.render_config_template(
            path=os.path.abspath(self.working_dir) + "/log/*"
        )

        pulse_proc = self.start_beat()
        self.wait_until(lambda: self.log_contains("pulse is running"))
        exit_code = pulse_proc.kill_and_wait()
        assert exit_code == 0
