# Pulse

Welcome to Pulse.

Ensure that this folder is at the following location:
`${GOPATH}/gitlab.com/perigee/pulse`


## Getting Started with Pulse

### Requirements

* [Golang](https://golang.org/dl/) 1.7
* python3

For working on project, need run

```
make
```

to have basic setup.



### Workflow

- Modify the Configuration under `_meta/beat.yml`


### Init Project
To get running with Pulse and also install the
dependencies, run the following command:

```
make setup
```

It will create a clean git history for each major step. Note that you can always rewrite the history if you wish before pushing your changes.

To push Pulse in the git repository, run the following commands:

```
git remote set-url origin https://gitlab.com/perigee/pulse
git push origin master
```

For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).

### Build

To build the binary for Pulse run the command below. This will generate a binary
in the same directory with the name pulse.

```
make
```


### Run

To run Pulse with debugging output enabled, run:

```
./pulse -c pulse.yml -e -d "*"
```


### Test

To test Pulse, run the following command:

```
make testsuite
```

alternatively:
```
make unit-tests
make system-tests
make integration-tests
make coverage-report
```

The test coverage is reported in the folder `./build/coverage/`

### Update

Each beat has a template for the mapping in elasticsearch and a documentation for the fields
which is automatically generated based on `etc/fields.yml`.
To generate etc/pulse.template.json and etc/pulse.asciidoc

```
make update
```


### Cleanup

To clean  Pulse source code, run the following commands:

```
make fmt
make simplify
```

To clean up the build directory and generated artifacts, run:

```
make clean
```


### Clone

To clone Pulse from the git repository, run the following commands:

```
mkdir -p ${GOPATH}/gitlab.com/perigee/pulse
cd ${GOPATH}/gitlab.com/perigee/pulse
git clone https://gitlab.com/perigee/pulse
```


For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).


## Packaging

The beat frameworks provides tools to crosscompile and package your beat for different platforms. This requires [docker](https://www.docker.com/) and vendoring as described above. To build packages of your beat, run the following command:

```
make package
```

This will fetch and create all images required for the build process. The hole process to finish can take several minutes.



